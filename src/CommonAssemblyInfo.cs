using System.Reflection;
using System.Runtime.InteropServices;
[assembly: AssemblyDescription("IoC Container for .Net")]
[assembly: AssemblyProduct("StructureMap")]
[assembly: AssemblyCopyright("Copyright 2004-2015 Jeremy D. Miller, Joshua Flanagan, Frank Quednau, Tim Kellogg, et al. All rights reserved.")]
[assembly: AssemblyTrademark("64c70e5d0c5aeeedbe26d24b3a1feb20d118d7fa")]
[assembly: AssemblyVersion("3.1.6.51611")]
[assembly: AssemblyFileVersion("3.1.6.51611")]
[assembly: AssemblyInformationalVersion("3.1.6.51611")]
